# Smart Cities Demo

Demo for working with Smart Cities Hackathon JSON API.

## Data Sets

http://pdx.datadash.io/smart-cities/report-data

## Users

  * Miles
  * Gage
  * Dustin
  * Griffin
  * Feeyee

### Date

The smart cities hackathon is may 16th.