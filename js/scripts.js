console.log('smart cities code!');

(function ( $ ) {

	var maxEntries = 100;
	var deqExportApiUrl = 'http://pdx.datadash.io/api/data/552d2900d838c1a200fa6925?limit=' + maxEntries;

	$(document).ready( function() {
		// get API data
		$.ajax({
			url: deqExportApiUrl,
			success: initApp,
			dataType: 'json'
		});
	});

	function initApp(data) {
	  console.log('data = ');
	  console.dir(data);

	  writeWindSpeedTable(data);
	}

	function writeWindSpeedTable(data) {
		var str = "<table class='table'>";
		str += "<tr>";
		str += "<th>EPA Station</th>";
		str += "<th>Wind Speed</th>";
		str += "<th>Wind Direction</th>";
		str += "</tr>";

		for (var i = 0; i < data.length; i++) {
			if ( data[i].hasOwnProperty('epa_station_key') && data[i].hasOwnProperty('resultant_speed') && data[i].epa_station_key != "" && data[i].resultant_speed != "" ) {
				str += "<tr>";
				str += "<td>" + data[i].epa_station_key + "</td>";
				str += "<td>" + makeWindSock( data[i].resultant_speed ) + "</td>";
				str += "<td>" + makeWindArrow( data[i].resultant_direction ) + "</td>";
				str += "</tr>";
			}
		}

		str += "</table>";

		$('#content').append(str);
	}

	function makeWindSock( speed ) {
		var windSockWidth = Math.round( speed * 10 );
		return "<div class='windsock' style='border-left:" + windSockWidth + "px solid orange'></div>";
	}
	function makeWindArrow(direction){
		return "<span class='windArrow' style='transform: rotate("+direction+"deg);'>&#8607;</span>"
	}

}( jQuery ));


